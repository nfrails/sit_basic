class CreateEnumerations < ActiveRecord::Migration
  def change
    create_table :enumerations do |t|
      t.string :name
      t.string :type

      t.timestamps null: false
    end
  end
end
