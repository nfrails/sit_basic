class CreatePhones < ActiveRecord::Migration
  def change
    create_table :phones do |t|
      t.integer :user_id
      t.string :phone
      t.integer :phone_type_id
      t.belongs_to :user, index: true, foreign_key: true
    end
  end
end
